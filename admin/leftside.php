<div class="left-side sticky-left-side">

        <!--logo and iconic logo start-->
        <div class="logo">
            <a href="<?=$_SESSION['url'] ?>"><img src="<?=$_SESSION['url'] ?>/html/images/logoa.png" alt="Bendajaya"></a>
        </div>

        <div class="logo-icon text-center">
            <a href="<?=$_SESSION['url'] ?>"><img src="<?=$_SESSION['url'] ?>/html/images/logo_icon.png" alt=""></a>
        </div>
        <!--logo and iconic logo end-->

        <div class="left-side-inner">

            <!-- visible to small devices only -->
            <div class="visible-xs hidden-sm hidden-md hidden-lg">
                <div class="media logged-user">
                    <img alt="" src="<?=$_SESSION['url'] ?>/html/images/photos/user-avatar.png" class="media-object">
                    <div class="media-body">
                        <h4><a href="#">John Doe</a></h4>
                        <span>"Hello There..."</span>
                    </div>
                </div>

                <h5 class="left-nav-title">Account Information</h5>
                <ul class="nav nav-pills nav-stacked custom-nav">
                  <li><a href="#"><i class="fa fa-user"></i> <span>Profile</span></a></li>
                  <li><a href="#"><i class="fa fa-cog"></i> <span>Settings</span></a></li>
                  <li><a href="#"><i class="fa fa-sign-out"></i> <span>Sign Out</span></a></li>
                </ul>
            </div>

            <!--sidebar nav start-->
            <ul class="nav nav-pills nav-stacked custom-nav">
                <li <?php if($mainword=="admin"){print("class=\"active\"");}?> ><a href="<?=$_SESSION['url'] ?>"><i class="fa fa-home"></i> <span>Dashboard</span></a></li>

                <?php if($_SESSION['level']==4){ ?>
                <!-- MENU ADMIN -->

                <li class="menu-list <?php if( $mainword=="transaksijualbeli" ){print("active");}?> "><a href=""><i class="fa fa-shopping-cart"></i> <span>Transaksi</span></a>
                    <ul class="sub-menu-list">
                        <li <?php if( $mainword=="transaksijualbeli"){print("class=\"active\"");}?> ><a href="<?=$_SESSION['url'] ?>modul/transaksijualbeli/">Transaksi Jual Beli</a></li>

                    </ul>
                </li>

                <li class="menu-list <?php if( $mainword=="laporankomplain" ){print("active");}?> "><a href=""><i class="fa fa-book"></i> <span>Laporan</span></a>
                    <ul class="sub-menu-list">
                        <li <?php if( $mainword=="laporankomplain"){print("class=\"active\"");}?> ><a href="<?=$_SESSION['url'] ?>modul/laporankomplain/">Komplain</a></li>

                    </ul>
                </li>


                <!-- // CALON NONAKTIF
                <li class="menu-list <?php if( $mainword=="komposisipempekkapalselam"  or $mainword=="komposisipempeklenjer"  or $mainword=="komposisipempekadaan"  or $mainword=="komposisipempektekwan"  or $mainword=="komposisipempeklenggang"  or $mainword=="komposisipempekkates" ){print("active");}?> "><a href=""><i class="fa fa-lemon-o"></i> <span>Komposisi Pempek</span></a>
                    <ul class="sub-menu-list">
                        <li <?php if($mainword=="komposisipempekkapalselam"){print("class=\"active\"");}?> ><a href="<?=$_SESSION['url'] ?>modul/komposisipempek/?idb=1">Kapal Selam</a></li>
                        <li <?php if($mainword=="komposisipempeklenjer"){print("class=\"active\"");}?> ><a href="<?=$_SESSION['url'] ?>modul/komposisipempek/?idb=2">Lenjer</a></li>
                        <li <?php if($mainword=="komposisipempekadaan"){print("class=\"active\"");}?> ><a href="<?=$_SESSION['url'] ?>modul/komposisipempek/?idb=3">Adaan</a></li>
                        <li <?php if($mainword=="komposisipempektekwan"){print("class=\"active\"");}?> ><a href="<?=$_SESSION['url'] ?>modul/komposisipempek/?idb=4">Tekwan</a></li>
                        <li <?php if($mainword=="komposisipempeklenggang"){print("class=\"active\"");}?> ><a href="<?=$_SESSION['url'] ?>modul/komposisipempek/?idb=5">Lenggang</a></li>
                        <li <?php if($mainword=="komposisipempekkates"){print("class=\"active\"");}?> ><a href="<?=$_SESSION['url'] ?>modul/komposisipempek/?idb=6">Kates</a></li>
<?php } ?>
                    </ul>
                </li>





                <!--
                <li class="menu-list"><a href=""><i class="fa fa-laptop"></i> <span>Layouts</span></a>
                    <ul class="sub-menu-list">
                        <li><a href="html/blank_page.html"> Blank Page</a></li>
                        <li><a href="html/boxed_view.html"> Boxed Page</a></li>
                        <li><a href="html/leftmenu_collapsed_view.html"> Sidebar Collapsed</a></li>
                        <li><a href="html/horizontal_menu.html"> Horizontal Menu</a></li>

                    </ul>
                </li>
                <li class="menu-list"><a href=""><i class="fa fa-book"></i> <span>UI Elements</span></a>
                    <ul class="sub-menu-list">
                        <li><a href="html/general.html"> General</a></li>
                        <li><a href="html/buttons.html"> Buttons</a></li>
                        <li><a href="html/tabs-accordions.html"> Tabs & Accordions</a></li>
                        <li><a href="html/typography.html"> Typography</a></li>
                        <li><a href="html/slider.html"> Slider</a></li>
                        <li><a href="html/panels.html"> Panels</a></li>
                    </ul>
                </li>
                <li class="menu-list"><a href=""><i class="fa fa-cogs"></i> <span>Components</span></a>
                    <ul class="sub-menu-list">
                        <li><a href="html/grids.html"> Grids</a></li>
                        <li><a href="html/gallery.html"> Media Gallery</a></li>
                        <li><a href="html/calendar.html"> Calendar</a></li>
                        <li><a href="html/tree_view.html"> Tree View</a></li>
                        <li><a href="html/nestable.html"> Nestable</a></li>

                    </ul>
                </li>

                <li><a href="html/fontawesome.html"><i class="fa fa-bullhorn"></i> <span>Fontawesome</span></a></li>

                <li class="menu-list"><a href=""><i class="fa fa-envelope"></i> <span>Mail</span></a>
                    <ul class="sub-menu-list">
                        <li><a href="html/mail.html"> Inbox</a></li>
                        <li><a href="html/mail_compose.html"> Compose Mail</a></li>
                        <li><a href="html/mail_view.html"> View Mail</a></li>
                    </ul>
                </li>

                <li class="menu-list"><a href=""><i class="fa fa-tasks"></i> <span>Forms</span></a>
                    <ul class="sub-menu-list">
                        <li><a href="html/form_layouts.html"> Form Layouts</a></li>
                        <li><a href="html/form_advanced_components.html"> Advanced Components</a></li>
                        <li><a href="html/form_wizard.html"> Form Wizards</a></li>
                        <li><a href="html/form_validation.html"> Form Validation</a></li>
                        <li><a href="html/editors.html"> Editors</a></li>
                        <li><a href="html/inline_editors.html"> Inline Editors</a></li>
                        <li><a href="html/pickers.html"> Pickers</a></li>
                        <li><a href="html/dropzone.html"> Dropzone</a></li>
                    </ul>
                </li>
                <li class="menu-list"><a href=""><i class="fa fa-bar-chart-o"></i> <span>Charts</span></a>
                    <ul class="sub-menu-list">
                        <li><a href="html/flot_chart.html"> Flot Charts</a></li>
                        <li><a href="html/morris.html"> Morris Charts</a></li>
                        <li><a href="html/chartjs.html"> Chartjs</a></li>
                        <li><a href="html/c3chart.html"> C3 Charts</a></li>
                    </ul>
                </li>
                <li class="menu-list"><a href="#"><i class="fa fa-th-list"></i> <span>Data Tables</span></a>
                    <ul class="sub-menu-list">
                        <li><a href="html/basic_table.html"> Basic Table</a></li>
                        <li><a href="html/dynamic_table.html"> Advanced Table</a></li>
                        <li><a href="html/responsive_table.html"> Responsive Table</a></li>
                        <li><a href="html/editable_table.html"> Edit Table</a></li>
                    </ul>
                </li>

                <li class="menu-list"><a href="#"><i class="fa fa-map-marker"></i> <span>Maps</span></a>
                    <ul class="sub-menu-list">
                        <li><a href="html/google_map.html"> Google Map</a></li>
                        <li><a href="html/vector_map.html"> Vector Map</a></li>
                    </ul>
                </li>
                <li class="menu-list"><a href=""><i class="fa fa-file-text"></i> <span>Extra Pages</span></a>
                    <ul class="sub-menu-list">
                        <li><a href="html/profile.html"> Profile</a></li>
                        <li><a href="html/invoice.html"> Invoice</a></li>
                        <li><a href="html/pricing_table.html"> Pricing Table</a></li>
                        <li><a href="html/timeline.html"> Timeline</a></li>
                        <li><a href="html/blog_list.html"> Blog List</a></li>
                        <li><a href="html/blog_details.html"> Blog Details</a></li>
                        <li><a href="html/directory.html"> Directory </a></li>
                        <li><a href="html/chat.html"> Chat </a></li>
                        <li><a href="html/404.html"> 404 Error</a></li>
                        <li><a href="html/500.html"> 500 Error</a></li>
                        <li><a href="html/registration.html"> Registration Page</a></li>
                        <li><a href="html/lock_screen.html"> Lockscreen </a></li>
                    </ul>
                </li>
                <li><a href="html/login.html"><i class="fa fa-sign-in"></i> <span>Login Page</span></a></li>
-->
            </ul>
            <!--sidebar nav end-->

        </div>
    </div>
