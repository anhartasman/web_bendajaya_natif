

<section>
    <!-- left side start-->
    <?php include("../../leftside.php"); ?>
    <!-- left side end-->
    
    <!-- main content start-->
    <div class="main-content" >

        <!-- header section start-->
        <?php include("../../header.php"); ?>
        <!-- header section end-->


        <!--body wrapper start-->
		  <div class="wrapper">
		  

            <div class="row">
                <div class="col-md-4">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel">
                                <div class="panel-body">
                                    <div class="profile-pic text-center">
                                        <img alt="" src="images/photos/user1.png">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="panel">
                                <div class="panel-body">
                                    <ul class="p-info">
                                        <li>
                                            <div class="title">Gender</div>
                                            <div class="desk"><?=$dataprofil['gender'];?></div>
                                        </li>
                                        <li>
                                            <div class="title">Work Place</div>
                                            <div class="desk"><?=$tempatkerja['nama']?></div>
                                        </li>
                                        <li>
                                            <div class="title">Email</div>
                                            <div class="desk"><?=$dataprofil['email']?></div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
						<!--
                        <div class="col-md-12">
                            <div class="panel">
                                <div class="panel-body p-states">
                                    <div class="summary pull-left">
                                        <h4>Total <span>Sales</span></h4>
                                        <span>Monthly Summary</span>
                                        <h3>$ 5,600</h3>
                                    </div>
                                    <div id="expense" class="chart-bar"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="panel">
                                <div class="panel-body p-states green-box">
                                    <div class="summary pull-left">
                                        <h4>Product <span>refund</span></h4>
                                        <span>Monthly Summary</span>
                                        <h3>160</h3>
                                    </div>
                                    <div id="pro-refund" class="chart-bar"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="panel">
                                <div class="panel-body p-states">
                                    <div class="summary pull-left">
                                        <h4>Total <span>Earning</span></h4>
                                        <span>Monthly Summary</span>
                                        <h3>$ 51,2600</h3>
                                    </div>
                                    <div id="expense2" class="chart-bar"></div>
                                </div>
                            </div>
                        </div>
						-->
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel">
                                <div class="panel-body">
                                    <div class="profile-desk">
                                        <h1><?=$dataprofil['first_name']." ".$dataprofil['last_name']?></h1>
                                        <span class="designation"><?=$_SESSION["pangkatuser"]?></span>
                                        <p>
                                            <?=$dataprofil['keterangan']?>
                                        </p>
										<!--
                                        <a class="btn p-follow-btn pull-left" href="#"> <i class="fa fa-check"></i> Following</a>

                                        <ul class="p-social-link pull-right">
                                            <li>
                                                <a href="#">
                                                    <i class="fa fa-facebook"></i>
                                                </a>
                                            </li>
                                            <li class="active">
                                                <a href="#">
                                                    <i class="fa fa-twitter"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <i class="fa fa-google-plus"></i>
                                                </a>
                                            </li>
                                        </ul>
										-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
					<!--
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel">
                                <form>
                                    <textarea class="form-control input-lg p-text-area" rows="2" placeholder="Whats in your mind today?"></textarea>
                                </form>
                                <footer class="panel-footer">
                                    <button class="btn btn-post pull-right">Post</button>
                                    <ul class="nav nav-pills p-option">
                                        <li>
                                            <a href="#"><i class="fa fa-user"></i></a>
                                        </li>
                                        <li>
                                            <a href="#"><i class="fa fa-camera"></i></a>
                                        </li>
                                        <li>
                                            <a href="#"><i class="fa  fa-location-arrow"></i></a>
                                        </li>
                                        <li>
                                            <a href="#"><i class="fa fa-meh-o"></i></a>
                                        </li>
                                    </ul>
                                </footer>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel">
                                <header class="panel-heading">
                                    recent activities
                                    <span class="tools pull-right">
                                        <a class="fa fa-chevron-down" href="javascript:;"></a>
                                        <a class="fa fa-times" href="javascript:;"></a>
                                     </span>
                                </header>
                                <div class="panel-body">
                                    <ul class="activity-list">
                                        
										
										<li>
                                            <div class="avatar">
                                                <img src="images/photos/user1.png" alt=""/>
                                            </div>
                                            <div class="activity-desk">
                                                <h5><a href="#">Jonathan Smith</a> <span>Uploaded 5 new photos</span></h5>
                                                <p class="text-muted">7 minutes ago near Alaska, USA</p>
                                                <div class="album">
                                                    <a href="#">
                                                        <img alt="" src="images/gallery/image1.jpg">
                                                    </a>
                                                    <a href="#">
                                                        <img alt="" src="images/gallery/image2.jpg">
                                                    </a>
                                                    <a href="#">
                                                        <img alt="" src="images/gallery/image3.jpg">
                                                    </a>
                                                </div>
                                            </div>
                                        </li>
										

                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
					-->
                </div>
            </div>

        </div>
        <!--body wrapper end-->

        <!--footer section start-->
        <!--<footer>
            <!--2014 &copy; AdminEx by ThemeBucket
        </footer>
        <!--footer section end-->


    </div>
    <!-- main content end-->
</section>
 
<script>
function setKonfirm(str){ 
	window.location="./?konfirm="+str;
}
</script>