

<section>
    <!-- left side start-->
    <?php include("../../leftside.php"); ?>
    <!-- left side end-->

    <!-- main content start-->
    <div class="main-content" >

        <!-- header section start-->
        <?php include("../../header.php"); ?>
        <!-- header section end-->

        <!-- page heading start-->
        <div class="page-heading">
            <h3>
                Dashboard <?=$_SESSION['userpempek'];?>
            </h3>
            <ul class="breadcrumb">
                <li>
                    <a href="<?=$_SESSION['url'] ?>">Dashboard</a>
                </li>
                <li class="active">Laporan Komplain</li>
            </ul>
        </div>
        <!-- page heading end-->

        <!--body wrapper start-->
        <div class="wrapper">
        <div class="row">
        <div class="col-sm-12">
        <section class="panel">
        <header class="panel-heading">
            Tabel Laporan Komplain</header>
        <div class="panel-body">
        <div class="adv-table">
        <?php
if($_SESSION['level']==4){


?>
                                    <table  class="display table table-bordered table-striped" id="dynamic-table">
        <thead>
          <tr>
          <th>#</th>
            <th>Tanggal</th>

            <th>User</th>

            <th>Judul Pesan</th>
            <th>Isi Pesan</th>
          </tr>
        </thead>
        <tbody>
      <?php
	  $nom=0;

	 $cari=$db->from('tb_complain') 
    ->sortAsc('create_at')
    ->many();

	foreach($cari as $hasil) {
	$nom+=1;
  $tb_user=$db->from('tb_user')
	   ->where('id',$hasil['idu'])
    ->one();

?>
<tr class="gradeX">
  <td><?=$nom?></td>
<td><?=date('d-m-Y H:i:s',strtotime($hasil["create_at"]))?></td>
  <td><?=$tb_user['name']?></td>
  <td><?=$hasil['message_title']?></td>
  <td><?=$hasil['message_body']?></td>
</tr>
        <?php

       }
       ?>
        </tbody>
        <tfoot>

        </tfoot>
        </table>
                                    <?php

}?>

        </div>
        </div>
        </section>
        </div>
        </div>
        <div class="row"></div>
        </div>
        <!--body wrapper end-->

        <!--footer section start-->
        <!--<footer>
            <!--2014 &copy; AdminEx by ThemeBucket
        </footer>
        <!--footer section end-->


    </div>
    <!-- main content end-->
</section>
<script>
function setIdb(str){
	window.location="./?idb="+str;
}
</script>
