
<section>
    <!-- left side start-->
    <?php
	if($datainvoice['level']==5){
$mainword="daftartransaksimakananmasterstokis";
$idb=1;
}
if($datainvoice['level']==6){
$mainword="daftartransaksimakananstokis";
$idb=2;
}
if($datainvoice['level']==8){
$mainword="daftartransaksimakananoutlet";	
$idb=3;
}
include("../../../leftside.php"); ?>
    <!-- left side end-->

    <!-- main content start-->
    <div class="main-content" >

        <!-- header section start-->
        <?php include("../../../header.php");?>
        <!-- header section end-->

        <!-- page heading start-->
        <div class="page-heading">
            <h3>
                Transaksi Makanan </h3>
            <ul class="breadcrumb">
                <li>
                    <a href="<?=$_SESSION['url']?>">Dashboard</a>
                </li>
                <li>
                    <a href="<?=$_SESSION['url']."modul/daftartransaksimakanan/?idb=$idb"?>"><?php
                if($idb==1){
print("Daftar Pengajuan Makanan");
}else if($idb==2){
print("Makanan Dalam Perjalanan");
}else if($idb==3){
print("Makanan Sudah Diterima");
}
			  ?></a>
                </li>
                <li class="active"> Detail</li>
            </ul>
        </div>
        <!-- page heading end-->

        <!--body wrapper start-->
        <section class="wrapper">
         <?php

	$datamakanan = $db->from('makanan')
    ->where('id', $datapemesanan['pempek'])
    ->one();

	$jumlahporsimakanan=$datamakanan['jumlahporsi'];

					  $butuhtabel=1;
					  $jumlahorder=$datapemesanan['jumlah'];
					  //$jumlahporsi=$datapemesanan['jumlah'];
					  $jumlahpakmakanan=ceil($datapemesanan['jumlah']/$jumlahporsimakanan);
					  $idmakanan=$datapemesanan['pempek'];
					  $onemenu=1;
					  include("../../../hargamakanan.php");

					  $biayaproduksi=$daftarmakanan_hargadasarmakanan[$idmakanan]*$jumlahpakmakanan;
					  $keuntungan=$datapemesanan['biaya']-$biayaproduksi;
					  ?>
            <div class="panel">

                     <?php

			if(isset($_GET['slh'])){

			print($_GET['psnslh']);
			}
			?>
                <div class="panel-body invoice">
                    <div class="row">
                        <div class="col-md-4 col-sm-4">
                            <h1 class="invoice-title">Order <?=$datamakanan['nama'];?> (<?=$datapemesanan['jumlah']?>)</h1>
                        </div>
                        <div class="col-md-4 col-md-offset-4 col-sm-4 col-sm-offset-4">
                            <img class="inv-logo" src="<?=$_SESSION['url'] ?>html/images/invoice-logo.jpg" alt=""/>
                            <p><?=$datamasterstokis['alamat']?></p>
                        </div>
                    </div>
                    <div class="invoice-address">
                        <div class="row">
                            <div class="col-md-5 col-sm-5">
                                <h4 class="inv-to">Order From </h4>
                                <h2 class="corporate-id"><?=$datapemesanan['namastokis']?></h2>
                                <p>
                                    <?=$datastokis['telp']?>
                                </p>
                                <p>
                                    <?=$datastokis['alamat']?>
                                </p>

                            </div>
                            <div class="col-md-4 col-md-offset-3 col-sm-4 col-sm-offset-3">
                               <!-- <div class="inv-col"><span>Invoice#</span> 432134-A</div> -->
                                <div class="inv-col"><span>Invoice Date :</span> <?=date('d-M-Y H:i:s',strtotime($datapemesanan['tanggalpesan']));?></div>
                                <h1 class="t-due">TOTAL</h1>
                                <h2 class="amnt-value">Rp. <?=formatrupiah($datapemesanan['biaya']);?></h2>
                            </div>
                        </div>
                    </div>
                </div>
                <table class="table table-bordered table-invoice">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Keterangan Bahan </th>
                        <th class="text-center">Unit Cost</th>
                        <th class="text-center">Quantity</th>
                        <th class="text-center">Total</th>
                    </tr>
                    </thead>
                    <tbody>

                      <?=$bahantabel?>
                    <tr>
                        <td colspan="2" class="payment-method">
                            <h4>Penjelasan Transaksi</h4>
                            <p>1. Sub Total adalah harga bahan yang dibutuhkan untuk membuat makanan.</p>
                            <p>2. Penambahan harga adalah keuntungan yang ditujukan untuk Master Stokis.</p>
                            <p>3. Grand Total adalah total biaya yang dipotong dari saldo stokis.</p>
                            <br>
                            <h3 class="inv-label">Thank you for your business</h3>
                        </td>
                        <td class="text-right" colspan="2">
                            <p>Sub Total</p>
                            <p>Penambahan Harga</p>
                            <p><strong>GRAND TOTAL</strong></p>
                        </td>
                        <td class="text-center">
						<?php

	  $datapenambahanhargastokis=$db->from('penambahanhargamakananstokis')
	  ->where('idmakanan',$hasiltabelmakanan['id'])
    ->one();
	$penambahanharga+=$datapenambahanhargastokis['penambahanharga'];
	?>
                            <p><?=formatrupiah($daftarmakanan_hargadasarsatuanmakanan[$datamakanan['id']]*$datapemesanan['jumlah'])?></p>
                            <p><?=formatrupiah($penambahanharga*$datapemesanan['jumlah'])?></p>
                            <p><strong><?=formatrupiah($datapemesanan['biaya'])?></strong></p>
                        </td>
                    </tr>

                    </tbody>
                </table>
            </div>
            <div class="text-center ">
                    <form id="input" method="post" class="form-horizontal foto_banyak" action="daftartransaksimakanan_konfirm.php">
						<?php  if($_SESSION['level'] ==5 and $datapemesanan['konfirm']==0){  ?>
						<input name="id" type="hidden" id="id" value="<?=$id?>" /><input name="idb" type="hidden" id="idb" value="<?=$idb?>" />
<div class="form-group">
                  <label for="tanggalkirim" class="control-label col-lg-2">Tanggal produksi</label>
                        <div class="col-lg-10">
                          <input type="text" id="tgl1" value="<?php print(date('Y-m-d H:i:s'));?>"   name="tanggalkirim" placeholder="tanggalkirim" class="form_datetime  form-control form-control-inline input-medium " required>
                        </div>
                     </div><!-- /.form-group -->
                <a class="btn btn-success btn-lg"onclick="$('#tomkonfirm').fadeIn();" ><i class="fa fa-check"></i> Submit Invoice </a>
                          <input name="submit" type="submit" class="btn btn-success btn-lg" id="tomkonfirm" value="konfirmasi" />
                <a class="btn btn-primary btn-lg" target="_blank" href="invoice_print.html"><i class="fa fa-print"></i> Print </a>

						  <?php } ?>
				</form>
            </div>
        </section>
		<br /><br /><br /><br /><br /><br /><br />
        <!--body wrapper end-->

        <!--footer section start-->
        <!--<footer>
            <!--2014 &copy; AdminEx by ThemeBucket
        </footer>
        <!--footer section end-->


    </div>
    <!-- main content end-->
</section>
