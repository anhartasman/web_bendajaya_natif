<?php
include("../../../koneksiSQL.php");
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="keywords" content="admin, dashboard, bootstrap, template, flat, modern, theme, responsive, fluid, retina, backend, html5, css, css3">
  <meta name="description" content="">
  <meta name="author" content="ThemeBucket">
  <link rel="shortcut icon" href="#" type="image/png">

  <title>AdminX</title>

  <!--icheck-->
  <link href="<?=$_SESSION['url'] ?>/html/js/iCheck/skins/minimal/minimal.css" rel="stylesheet">
  <link href="<?=$_SESSION['url'] ?>/html/js/iCheck/skins/square/square.css" rel="stylesheet">
  <link href="<?=$_SESSION['url'] ?>/html/js/iCheck/skins/square/red.css" rel="stylesheet">
  <link href="<?=$_SESSION['url'] ?>/html/js/iCheck/skins/square/blue.css" rel="stylesheet">

  <!--dashboard calendar-->
  <link href="<?=$_SESSION['url'] ?>/html/css/clndr.css" rel="stylesheet">

  <!--Morris Chart CSS -->
  <link rel="stylesheet" href="<?=$_SESSION['url'] ?>/html/js/morris-chart/morris.css">

  <!--common-->
  <link href="<?=$_SESSION['url'] ?>/html/css/style.css" rel="stylesheet">
  <link href="<?=$_SESSION['url'] ?>/html/css/style-responsive.css" rel="stylesheet">
  <link href="js/advanced-datatable/css/demo_page.css" rel="stylesheet" />
  <link href="js/advanced-datatable/css/demo_table.css" rel="stylesheet" />
  <link rel="stylesheet" href="js/data-tables/DT_bootstrap.css" />

  <link href="css/style.css" rel="stylesheet">
  <link href="css/style-responsive.css" rel="stylesheet">  
  <link href="<?=$_SESSION['url'] ?>/html/js/advanced-datatable/css/demo_table.css" rel="stylesheet" /> 

  <link rel="stylesheet" type="text/css" href="<?=$_SESSION['url'] ?>/html/js/bootstrap-datepicker/css/datepicker-custom.css" />
  <link rel="stylesheet" type="text/css" href="<?=$_SESSION['url'] ?>/html/js/bootstrap-timepicker/css/timepicker.css" />
  <link rel="stylesheet" type="text/css" href="<?=$_SESSION['url'] ?>/html/js/bootstrap-colorpicker/css/colorpicker.css" />
  <link rel="stylesheet" type="text/css" href="<?=$_SESSION['url'] ?>/html/js/bootstrap-daterangepicker/daterangepicker-bs3.css" />
  <link rel="stylesheet" type="text/css" href="<?=$_SESSION['url'] ?>/html/js/bootstrap-datetimepicker/css/datetimepicker-custom.css" />



  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="js/html5shiv.js"></script>
  <script src="js/respond.min.js"></script>
  <![endif]-->
</head>
<body class="sticky-header">

<?php
$id=$_GET['id'];
	  $datapemesanan=$db->from('riwayatpermintaanpempek')   
	  ->where('id',$id) 
    ->one();
	 
$tabelmakanan=$db->from('makanan')  
	  ->where('id',$datapemesanan['pempek'])  
    ->one();	
	
	if($datapemesanan['konfirm']==0){
$mainword="daftarpengajuanmakanan";	
$idb=1;
}
if($datapemesanan['konfirm']==1){
$mainword="transaksimakanandalamperjalanan";	
$idb=2;
}
if($datapemesanan['konfirm']==2){
$mainword="transaksimakananselesai";	
$idb=3;
} 
 $dataakun=$db->from('tb_user')   
	  ->where('id',$_SESSION['id_user']) 
    ->one();
	  $datamasterstokis=$db->from('stokis')   
	  ->where('id',$dataakun['idstokis']) 
    ->one(); 

	  $datastokis=$db->from('stokis')   
	  ->where('id',$datapemesanan['idstokis']) 
    ->one(); 

include("daftartransaksimakanan_view.php");
?>


<script src="<?=$_SESSION['url'] ?>/html/js/jquery-1.10.2.min.js"></script>
<script src="<?=$_SESSION['url'] ?>/html/js/jquery-ui-1.9.2.custom.min.js"></script>
<script src="<?=$_SESSION['url'] ?>/html/js/jquery-migrate-1.2.1.min.js"></script>
<script src="<?=$_SESSION['url'] ?>/html/js/bootstrap.min.js"></script>
<script src="<?=$_SESSION['url'] ?>/html/js/modernizr.min.js"></script>
<script src="<?=$_SESSION['url'] ?>/html/js/jquery.nicescroll.js"></script>

<!--pickers plugins-->
<script type="text/javascript" src="<?=$_SESSION['url'] ?>/html/js/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="<?=$_SESSION['url'] ?>/html/js/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript" src="<?=$_SESSION['url'] ?>/html/js/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="<?=$_SESSION['url'] ?>/html/js/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="<?=$_SESSION['url'] ?>/html/js/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<script type="text/javascript" src="<?=$_SESSION['url'] ?>/html/js/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>

<!--pickers initialization-->
<script src="<?=$_SESSION['url'] ?>/html/js/pickers-init.js"></script>


<!--common scripts for all pages-->
<script src="<?=$_SESSION['url'] ?>/html/js/scripts.js"></script>
 <script>
			$('#tomkonfirm').hide();
			</script>
</body>
</html>