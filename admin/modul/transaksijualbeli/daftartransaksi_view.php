

<section>
    <!-- left side start-->
    <?php include("../../leftside.php"); ?>
    <!-- left side end-->

    <!-- main content start-->
    <div class="main-content" >

        <!-- header section start-->
        <?php include("../../header.php"); ?>
        <!-- header section end-->

        <!-- page heading start-->
        <div class="page-heading">
            <h3>
                Dashboard <?=$_SESSION['userpempek'];?>
            </h3>
            <ul class="breadcrumb">
                <li>
                    <a href="<?=$_SESSION['url'] ?>">Dashboard</a>
                </li>
                <li class="active">Transaksi Jual Beli</li>
            </ul>
        </div>
        <!-- page heading end-->

        <!--body wrapper start-->
        <div class="wrapper">
        <div class="row">
        <div class="col-sm-12">
        <section class="panel">
        <header class="panel-heading">
            Table Transaksi Jual Beli</header>
        <div class="panel-body">
        <div class="adv-table">
        <?php
if($_SESSION['level']==4){


?>
<form method="get">
        <table width="523" class="table  ">
                                   <thead>
                                     <tr>

													<td width="79" align="right">Periode</td>
									   <td width="6"><select name="periodebulan" class="form-control m-bot15" tabindex="2" required="required" id="periodebulan">
													  <option value=""></option>
													  <?php
													  $bulan=array("","january","february",
"Maret","April","Mei","Juni","Juli",
"Agustus","September","Oktober",
"November","Desember");

 for($i=7;$i<=12;$i++) {


	if($i==$_GET['periodebulan']){
	print(" <option value=\"".$i."\"selected=\"selected\">".$bulan[$i]."</option> ");
	}else{
	print(" <option value=\"".$i."\">".$bulan[$i]."</option> ");
	}
               } ?>
								       </select></td>
													<td width="56"><select name="periodetahun" class="form-control m-bot15" tabindex="2" required="required" id="periodetahun">
													  <option value=""></option>
													  <?php for($i=2018;$i<=2019;$i++) {
               		if($i==$_GET['periodetahun']){
	print(" <option value=\"".$i."\"selected=\"selected\">".$i."</option> ");
	}else{
	print(" <option value=\"".$i."\">".$i."</option> ");
	}
               } ?>
												    </select></td>

                          <td width="40"><input onclick="cari()" class="btn btn-primary btn-flat"  type="submit" name="Submit" id="button" value="Cari" /></td>

                        </tr>
                                     <tr>
                                       <td>&nbsp;</td>
                                       <td>&nbsp;</td>
													<td>&nbsp;</td>
                        </tr>
                                  </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
</form>
                                    <table  class="display table table-bordered table-striped" id="dynamic-table">
        <thead>
          <tr>
          <th>#</th>
            <th>Tanggal</th>

            <th>User Pembeli</th>

            <th>User Penjual</th>
            <th>Biaya</th>
            <th>Pajak</th>
            <th>Ongkir</th>
            <th>Keuntungan</th>
            <th>Status</th>
          </tr>
        </thead>
        <tbody>
      <?php
	  $nom=0;

if(empty($_GET['periodebulan'])==false and empty($_GET['periodetahun'])==false){
	$awalbulan=$_GET['periodetahun'].'-'.$_GET['periodebulan'].'-01';
	$akhirbulan=$_GET['periodetahun'].'-'.$_GET['periodebulan'].'-31';
	 $cari=$db->from('tb_transaksi')
	  ->where('create_at BETWEEN "'.$awalbulan.'"')
	  ->where('and "'.$akhirbulan.'"')
    ->sortAsc('create_at')
    ->many();

$arstatus=array();
$arstatus[0]="Belum diantar";
$arstatus[1]="Sedang diantar";
$arstatus[2]="Sudah diterima";
$arstatus[3]="Dicancel";
$totalkeuntungan=0;
	foreach($cari as $hasil) {
	$nom+=1;
  $tb_buyer=$db->from('tb_user')
	   ->where('id',$hasil['idb'])
    ->one();
  $tb_seller=$db->from('tb_user')
  	  ->where('id',$hasil['ids'])
      ->one();

?>
<tr class="gradeX">
  <td><?=$nom?></td>
<td><?=date('d-m-Y H:i:s',strtotime($hasil["create_at"]))?></td>
  <td><?=$tb_buyer['name']?></td>
  <td><?=$tb_seller['name']?></td>
  <td><?=formatrupiah($hasil["bill"])?></td>
  <td><?=formatrupiah($hasil["tax"])?></td>
  <td><?=formatrupiah($hasil["ongkir"])?></td>
  <td><?=formatrupiah($hasil["tax"]+$hasil["ongkir"])?></td>
<td><?=$arstatus[$hasil['status']]?></td>
</tr>
        <?php
       $totalkeuntungan+=($hasil["tax"]+$hasil["ongkir"]);
       }
       ?>
        </tbody>
        <tfoot>
          <tr class="gradeX">
            <td></td>
          <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td><b><?=formatrupiah($totalkeuntungan)?></b></td>
          <td> </td>
          </tr>
        </tfoot>
        </table>
                                    <?php }

}?>

        </div>
        </div>
        </section>
        </div>
        </div>
        <div class="row"></div>
        </div>
        <!--body wrapper end-->

        <!--footer section start-->
        <!--<footer>
            <!--2014 &copy; AdminEx by ThemeBucket
        </footer>
        <!--footer section end-->


    </div>
    <!-- main content end-->
</section>
<script>
function setIdb(str){
	window.location="./?idb="+str;
}
</script>
