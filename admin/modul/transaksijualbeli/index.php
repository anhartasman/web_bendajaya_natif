<?php
include("../../koneksiSQL.php");
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="keywords" content="admin, dashboard, bootstrap, template, flat, modern, theme, responsive, fluid, retina, backend, html5, css, css3">
  <meta name="description" content="">
  <meta name="author" content="ThemeBucket">
  <link rel="shortcut icon" href="#" type="image/png">

  <title>AdminX</title>

  <!--icheck-->
  <link href="<?=$_SESSION['url'] ?>/html/js/iCheck/skins/minimal/minimal.css" rel="stylesheet">
  <link href="<?=$_SESSION['url'] ?>/html/js/iCheck/skins/square/square.css" rel="stylesheet">
  <link href="<?=$_SESSION['url'] ?>/html/js/iCheck/skins/square/red.css" rel="stylesheet">
  <link href="<?=$_SESSION['url'] ?>/html/js/iCheck/skins/square/blue.css" rel="stylesheet">

  <!--dashboard calendar-->
  <link href="<?=$_SESSION['url'] ?>/html/css/clndr.css" rel="stylesheet">

  <!--Morris Chart CSS -->
  <link rel="stylesheet" href="<?=$_SESSION['url'] ?>/html/js/morris-chart/morris.css">

  <!--common-->
  <link href="<?=$_SESSION['url'] ?>/html/css/style.css" rel="stylesheet">
  <link href="<?=$_SESSION['url'] ?>/html/css/style-responsive.css" rel="stylesheet">
  <link href="js/advanced-datatable/css/demo_page.css" rel="stylesheet" />
  <link href="js/advanced-datatable/css/demo_table.css" rel="stylesheet" />
  <link rel="stylesheet" href="js/data-tables/DT_bootstrap.css" />

  <link href="css/style.css" rel="stylesheet">
  <link href="css/style-responsive.css" rel="stylesheet">
  <link href="<?=$_SESSION['url'] ?>/html/js/advanced-datatable/css/demo_table.css" rel="stylesheet" />




  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="js/html5shiv.js"></script>
  <script src="js/respond.min.js"></script>
  <![endif]-->
</head>
<body class="sticky-header">

<?php
if(!empty($_GET['idb'])){
if($_GET['idb']==1){
$mainword="daftarpengajuanmakanan";
$konfirm=0;
}
if($_GET['idb']==2){
$mainword="transaksimakanandalamperjalanan";
$konfirm=1;
}
if($_GET['idb']==3){
$mainword="transaksimakananselesai";
$konfirm=2;
}
$idb=$_GET['idb'];
}
include("daftartransaksi_view.php");
?>


<!-- Placed js at the end of the document so the pages load faster -->
<script src="<?=$_SESSION['url'] ?>/html/js/jquery-1.10.2.min.js"></script>
<script src="<?=$_SESSION['url'] ?>/html/js/jquery-ui-1.9.2.custom.min.js"></script>
<script src="<?=$_SESSION['url'] ?>/html/js/jquery-migrate-1.2.1.min.js"></script>
<script src="<?=$_SESSION['url'] ?>/html/js/bootstrap.min.js"></script>
<script src="<?=$_SESSION['url'] ?>/html/js/modernizr.min.js"></script>
<script src="<?=$_SESSION['url'] ?>/html/js/jquery.nicescroll.js"></script>

<!--easy pie chart-->
<script src="<?=$_SESSION['url'] ?>/html/js/easypiechart/jquery.easypiechart.js"></script>
<script src="<?=$_SESSION['url'] ?>/html/js/easypiechart/easypiechart-init.js"></script>

<!--Sparkline Chart-->
<script src="<?=$_SESSION['url'] ?>/html/js/sparkline/jquery.sparkline.js"></script>
<script src="<?=$_SESSION['url'] ?>/html/js/sparkline/sparkline-init.js"></script>

<!--icheck -->
<script src="<?=$_SESSION['url'] ?>/html/js/iCheck/jquery.icheck.js"></script>
<script src="<?=$_SESSION['url'] ?>/html/js/icheck-init.js"></script>

<!-- jQuery Flot Chart-->
<script src="<?=$_SESSION['url'] ?>/html/js/flot-chart/jquery.flot.js"></script>
<script src="<?=$_SESSION['url'] ?>/html/js/flot-chart/jquery.flot.tooltip.js"></script>
<script src="<?=$_SESSION['url'] ?>/html/js/flot-chart/jquery.flot.resize.js"></script>


<!--Morris Chart-->
<script src="<?=$_SESSION['url'] ?>/html/js/morris-chart/morris.js"></script>
<script src="<?=$_SESSION['url'] ?>/html/js/morris-chart/raphael-min.js"></script>

<!--Calendar-->
<script src="<?=$_SESSION['url'] ?>/html/js/calendar/clndr.js"></script>
<script src="<?=$_SESSION['url'] ?>/html/js/calendar/evnt.calendar.init.js"></script>
<script src="<?=$_SESSION['url'] ?>/html/js/calendar/moment-2.2.1.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.5.2/underscore-min.js"></script>

<!--common scripts for all pages-->
<script src="<?=$_SESSION['url'] ?>/html/js/scripts.js"></script>

<!--Dashboard Charts-->
<script src="<?=$_SESSION['url'] ?>/html/js/dashboard-chart-init.js"></script>


<!--dynamic table-->
<script type="text/javascript" language="javascript" src="<?=$_SESSION['url'] ?>/html/js/advanced-datatable/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?=$_SESSION['url'] ?>/html/js/data-tables/DT_bootstrap.js"></script>
<!--dynamic table initialization -->
<script src="<?=$_SESSION['url'] ?>/html/js/dynamic_table_init.js"></script>




</body>
</html>
